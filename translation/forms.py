from django.db import models
from django import forms

class SourceForm(forms.Form):
    task_type_choices = (
        ('自动检测',0),
        ('文言文',1),
        ('现代文',2),
    )

    # 设置form表单的必填项
    task_type = forms.ChoiceField(label='任务类型:', widget=forms.Select(), choices=task_type_choices,initial=task_type_choices[0])
    comment = forms.CharField(label='备注',max_length=30)

class SelectSourceForm(models.Model):
    task_type = models.SmallIntegerField(default=0, choices=SourceForm.task_type_choices, verbose_name='任务类型')
    comment = models.CharField(max_length=30, verbose_name='备注信息')

    class Meta:
        db_table = 'dp_form_test'
        verbose_name = 'form表单测试'
        verbose_name_plural = verbose_name
        ordering = ['id'] # 排序字段